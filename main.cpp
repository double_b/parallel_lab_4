#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;


#define MATRIX_SIZE 500
#define MAX_RAND 100

void task1() {

    clock_t start;
    double duration;

    double A[MATRIX_SIZE][MATRIX_SIZE], B[MATRIX_SIZE][MATRIX_SIZE], C[MATRIX_SIZE][MATRIX_SIZE];


    for (int i = 0; i < MATRIX_SIZE; i++) {
        for (int j = 0; j < MATRIX_SIZE; j++) {
            A[i][j] = rand() % MAX_RAND;
            B[i][j] = rand() % MAX_RAND;
        }
    }


    start = std::clock();
    for (int i = 0; i < MATRIX_SIZE; ++i) {
        for (int j = 0; j < MATRIX_SIZE; ++j) {
            C[i][j] = 0;
            for (int k = 0; k < MATRIX_SIZE; ++k) {
                C[i][j] += A[i][k] * B[k][j];  //Sum of multiplication
            }
        }
    }

    duration = (clock() - start) / (double) CLOCKS_PER_SEC;

    cout << "Duration: " << duration << endl;
}

int main() {
    srand(static_cast<unsigned int>(time(nullptr)));
    task1();
    return 0;
}