#include <mpi.h>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>

using namespace std;
int ProcNum;
int ProcRank;
int Size = 500;
double *A;
double *B;
double *C;

void Rand(double *pMatrix, int Size) {
    srand(100);
    for (int i = 0; i < Size; i++) {
        for (int j = 0; j < Size; j++) {
            pMatrix[i * Size + j] = rand() / double(1000);
        }
    }
}

void MatrixMultiplicationMPI(double *&A, double *&B, double *&C, int &Size) {
    int dim = Size;
    int i, j, k, p, ind;
    double temp;
    MPI_Status Status;
    int ProcPartSize = dim / ProcNum;
    int ProcPartElem = ProcPartSize * dim;
    double *bufA = new double[dim * ProcPartSize];
    double *bufB = new double[dim * ProcPartSize];
    double *bufC = new double[dim * ProcPartSize];
    int ProcPart = dim / ProcNum, part = ProcPart * dim;
    if (ProcRank == 0) {
        double temp = 0.0;
        for (int i = 0; i < dim; i++) {
            for (int j = i + 1; j < dim; j++) {
                temp = B[i * dim + j];
                B[i * dim + j] = B[j * dim + i];
                B[j * dim + i] = temp;
            }
        }

    }
    /*
    разбивает на части от главного процесса на равные части и передает в буфер
    A - адрес буфера передачи;
    part - количество элементов, пересылаемых каждому процессу;
    MPI_DOUBLE - тип передаваемых данных;
    0 - количество элементов в буфере приема;
    MPI_DOUBLE - тип принимаемых данных;
    0 - ранг передающего процесса;
    MPI_COMM_WORLD - коммуникатор.
    */
    MPI_Scatter(A, part, MPI_DOUBLE, bufA, part, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatter(B, part, MPI_DOUBLE, bufB, part, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    temp = 0.0;
    for (i = 0; i < ProcPartSize; i++) {
        for (j = 0; j < ProcPartSize; j++) {
            for (k = 0; k < dim; k++) temp += bufA[i * dim + k] * bufB[j * dim + k];
            bufC[i * dim + j + ProcPartSize * ProcRank] = temp;
            temp = 0.0;
        }
    }
    int NextProc;
    int PrevProc;
    for (p = 1; p < ProcNum; p++) {
        NextProc = ProcRank + 1;
        if (ProcRank == ProcNum - 1) NextProc = 0;
        PrevProc = ProcRank - 1;
        if (ProcRank == 0) PrevProc = ProcNum - 1;
        /*получает и заменяет данные*/
        MPI_Sendrecv_replace(bufB, part, MPI_DOUBLE, NextProc, 0, PrevProc, 0, MPI_COMM_WORLD, &Status);
        temp = 0.0;
        for (i = 0; i < ProcPartSize; i++) {
            for (j = 0; j < ProcPartSize; j++) {
                for (k = 0; k < dim; k++) {
                    temp += bufA[i * dim + k] * bufB[j * dim + k];
                }
                if (ProcRank - p >= 0)
                    ind = ProcRank - p;
                else ind = (ProcNum - p + ProcRank);
                bufC[i * dim + j + ind * ProcPartSize] = temp;
                temp = 0.0;
            }
        }
    }
    /*производит сборку блоков данных, посылаемых всеми процессами группы,
    в один массив процесса с номером root*/
    MPI_Gather(bufC, ProcPartElem, MPI_DOUBLE, C, ProcPartElem, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    delete[]bufA;
    delete[]bufB;
    delete[]bufC;
}

int main(int argc, char *argv[]) {
    double startTime, endTime, parallel = 0;
    MPI_Init(&argc, &argv);
    ///Определение размера области взаимодействия
    MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);

    ///определение ранга процессора
    MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
    ///широковещательная рассылка данных, от одного процесса всем процессам программы
    MPI_Bcast(&Size, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (ProcRank == 0) {
        A = new double[Size * Size];
        B = new double[Size * Size];
        C = new double[Size * Size];
        Rand(A, Size);
        Rand(B, Size);
    }
    startTime = MPI_Wtime();
    MatrixMultiplicationMPI(A, B, C, Size);
    endTime = MPI_Wtime();
    parallel = endTime - startTime;
    if (ProcRank == 0) {
        cout << "Time: " << parallel << endl;
    }
    delete[] A;
    delete[] B;
    delete[] C;
    MPI_Finalize();
    return 0;
}